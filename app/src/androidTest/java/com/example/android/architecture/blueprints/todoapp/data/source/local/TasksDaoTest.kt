package com.example.android.architecture.blueprints.todoapp.data.source.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.android.architecture.blueprints.todoapp.data.Task
import kotlinx.coroutines.test.runTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.notNullValue
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@SmallTest
@RunWith(AndroidJUnit4::class)
class TasksDaoTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val database: ToDoDatabase by lazy {
        // Using an in-memory database so that the information stored here
        // disappears when the process is killed.
        Room.inMemoryDatabaseBuilder(
            getApplicationContext(),
            ToDoDatabase::class.java
        ).build()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertTaskAndGetById() = runTest {
        // GIVEN - Insert a task.
        val task = Task("title", "description")
        database.taskDao().insertTask(task)

        // WHEN - Get the task by id from the database.
        val dbTask = database.taskDao().getTaskById(task.id)

        // THEN - The loaded data contains the expected values.
        assertThat(dbTask, notNullValue())

        dbTask as Task
        assertThat(dbTask.id, `is`(task.id))
        assertThat(dbTask.title, `is`(task.title))
        assertThat(dbTask.description, `is`(task.description))
        assertThat(dbTask.isCompleted, `is`(task.isCompleted))
    }

    @Test
    fun updateTaskAndGetById() = runTest {
        // 1. Insert a task into the DAO.
        val originalTask = Task("title", "description")
        database.taskDao().insertTask(originalTask)

        // 2. Update the task by creating a new task with the same ID but different attributes.
        val updatedTask = originalTask.copy(isCompleted = true)
        database.taskDao().updateTask(updatedTask)

        // 3. Check that when you get the task by its ID, it has the updated values.
        val dbTask = database.taskDao().getTaskById(updatedTask.id)
        assertThat(dbTask, notNullValue())

        dbTask as Task
        assertThat(dbTask.id, `is`(originalTask.id))
        assertThat(dbTask.title, `is`(updatedTask.title))
        assertThat(dbTask.description, `is`(updatedTask.description))
        assertThat(dbTask.isCompleted, `is`(updatedTask.isCompleted))
    }
}
