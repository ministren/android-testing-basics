package com.example.android.architecture.blueprints.todoapp.util.rules

import android.graphics.Bitmap
import androidx.test.core.app.takeScreenshot
import androidx.test.services.storage.TestStorage
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import java.io.IOException

/**
 * adb pull "$(adb shell 'echo "$EXTERNAL_STORAGE"' | tr -d '\r')/googletest/test_outputfiles"
 */
class FailureScreenshotRule : TestWatcher() {

    override fun failed(e: Throwable, description: Description) {
        val parentFolderPath = "failures/${description.className}"
        val screenshotFileName = "${description.methodName}.png".replace(" ", "_")

        try {
            val screenshot = takeScreenshot()
            TestStorage()
                .openOutputFile("${parentFolderPath}/${screenshotFileName}")
                .use {
                    screenshot.compress(Bitmap.CompressFormat.PNG, 100, it)
                }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}
