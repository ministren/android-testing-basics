package com.example.android.architecture.blueprints.todoapp.util.rules

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.testing.FragmentScenario
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.IdlingRegistry
import com.example.android.architecture.blueprints.todoapp.util.DataBindingIdlingResource
import com.example.android.architecture.blueprints.todoapp.util.EspressoIdlingResource
import com.example.android.architecture.blueprints.todoapp.util.monitorActivity
import com.example.android.architecture.blueprints.todoapp.util.monitorFragment
import org.junit.AssumptionViolatedException
import org.junit.rules.TestRule
import org.junit.rules.TestWatcher
import org.junit.runner.Description

@Suppress("TestFunctionName")
fun EspressoIdlingResourceRule(): TestRule = EspressoIdlingResourceTestWatcher()

@Suppress("TestFunctionName")
fun DataBindingIdlingResourceRule(): DataBindingIdlingResourceRule =
    DataBindingIdlingResourceTestWatcher()

interface DataBindingIdlingResourceRule : TestRule {

    /**
     * Sets the activity from an [ActivityScenario] to be used from [DataBindingIdlingResource].
     */
    fun monitorActivity(activityScenario: ActivityScenario<out FragmentActivity>)

    /**
     * Sets the fragment from a [FragmentScenario] to be used from [DataBindingIdlingResource].
     */
    fun monitorFragment(fragmentScenario: FragmentScenario<Fragment>)
}

class AndroidIdlingResourceRule : TestWatcher() {

    private val espressoIdlingResourceRule: EspressoIdlingResourceTestWatcher =
        EspressoIdlingResourceRule() as EspressoIdlingResourceTestWatcher

    private val dataBindingIdlingResourceRule: DataBindingIdlingResourceTestWatcher =
        DataBindingIdlingResourceRule() as DataBindingIdlingResourceTestWatcher

    override fun succeeded(description: Description) {
        super.succeeded(description)
        espressoIdlingResourceRule.succeeded(description)
        dataBindingIdlingResourceRule.succeeded(description)
    }

    override fun failed(e: Throwable, description: Description) {
        super.failed(e, description)
        espressoIdlingResourceRule.failed(e, description)
        dataBindingIdlingResourceRule.failed(e, description)
    }

    override fun skipped(e: AssumptionViolatedException, description: Description) {
        super.skipped(e, description)
        espressoIdlingResourceRule.skipped(e, description)
        dataBindingIdlingResourceRule.skipped(e, description)
    }

    /**
     * Idling resources tell Espresso that the app is idle or busy.
     * This is needed when operations are not scheduled in the main Looper
     * (for example when executed on a different thread).
     */
    override fun starting(description: Description) {
        super.starting(description)
        espressoIdlingResourceRule.starting(description)
        dataBindingIdlingResourceRule.starting(description)
    }

    /**
     * Unregister your Idling Resource, so it can be garbage collected and does not leak any memory.
     */
    override fun finished(description: Description) {
        super.finished(description)
        espressoIdlingResourceRule.finished(description)
        dataBindingIdlingResourceRule.finished(description)
    }

    fun monitorActivity(activityScenario: ActivityScenario<out FragmentActivity>) {
        dataBindingIdlingResourceRule.monitorActivity(activityScenario)
    }

    fun monitorFragment(fragmentScenario: FragmentScenario<Fragment>) {
        dataBindingIdlingResourceRule.monitorFragment(fragmentScenario)
    }
}

private class EspressoIdlingResourceTestWatcher : TestWatcher() {

    public override fun succeeded(description: Description) =
        super.succeeded(description)

    public override fun failed(e: Throwable, description: Description) =
        super.failed(e, description)

    public override fun skipped(e: AssumptionViolatedException, description: Description) =
        super.skipped(e, description)

    public override fun starting(description: Description) {
        super.starting(description)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    public override fun finished(description: Description) {
        super.finished(description)
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }
}

private class DataBindingIdlingResourceTestWatcher : TestWatcher(), DataBindingIdlingResourceRule {

    /** An idling resource that waits for Data Binding to have no pending bindings. */
    private val dataBindingIdlingResource = DataBindingIdlingResource()

    public override fun succeeded(description: Description) =
        super.succeeded(description)

    public override fun failed(e: Throwable, description: Description) =
        super.failed(e, description)

    public override fun skipped(e: AssumptionViolatedException, description: Description) =
        super.skipped(e, description)

    public override fun starting(description: Description) {
        super.starting(description)
        IdlingRegistry.getInstance().register(dataBindingIdlingResource)
    }

    public override fun finished(description: Description) {
        super.finished(description)
        IdlingRegistry.getInstance().unregister(dataBindingIdlingResource)
    }

    override fun monitorActivity(activityScenario: ActivityScenario<out FragmentActivity>) {
        dataBindingIdlingResource.monitorActivity(activityScenario)
    }

    override fun monitorFragment(fragmentScenario: FragmentScenario<Fragment>) {
        dataBindingIdlingResource.monitorFragment(fragmentScenario)
    }
}
