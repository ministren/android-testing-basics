package com.example.android.architecture.blueprints.todoapp.statistics

import com.example.android.architecture.blueprints.todoapp.data.Task
import org.junit.Assert.assertEquals
import org.junit.Test

class StatisticsUtilsTestJUnit {

    @Test
    fun getActiveAndCompletedStats_noCompleted_returnsHundredZero() {
        // Create an active task
        val tasks = listOf<Task>(
            Task("Title", "Description", isCompleted = false),
        )

        // Call your function
        val result = getActiveAndCompletedStats(tasks)

        // Check the result
        assertEquals(result.activeTasksPercent, 100f)
        assertEquals(result.completedTasksPercent, 0f)

//        assertSame(result.completedTasksPercent, 0f)
    }

    @Test
    fun getActiveAndCompletedStats_noActive_returnsZeroHundred() {
        val tasks = listOf(
            Task("Title", "Description", isCompleted = true),
        )
        val result = getActiveAndCompletedStats(tasks)
        assertEquals(result.activeTasksPercent, 0f)
        assertEquals(result.completedTasksPercent, 100f)
    }

    @Test
    fun getActiveAndCompletedStats_both_returnsSixtyForty() {
        val tasks = listOf(
            Task("Title", "Description", isCompleted = true),
            Task("Title", "Description", isCompleted = true),
            Task("Title", "Description", isCompleted = false),
            Task("Title", "Description", isCompleted = false),
            Task("Title", "Description", isCompleted = false),
        )
        val result = getActiveAndCompletedStats(tasks)
        assertEquals(result.activeTasksPercent, 60f)
        assertEquals(result.completedTasksPercent, 40f)
    }

    @Test
    fun getActiveAndCompletedStats_empty_returnsZeros() {
        val tasks = emptyList<Task>()
        val result = getActiveAndCompletedStats(tasks)
        assertEquals(result.activeTasksPercent, 0f)
        assertEquals(result.completedTasksPercent, 0f)
    }

    @Test
    fun getActiveAndCompletedStats_error_returnsZeros() {
        val tasks: List<Task>? = null
        val result = getActiveAndCompletedStats(tasks)
        assertEquals(result.activeTasksPercent, 0f)
        assertEquals(result.completedTasksPercent, 0f)
    }
}
