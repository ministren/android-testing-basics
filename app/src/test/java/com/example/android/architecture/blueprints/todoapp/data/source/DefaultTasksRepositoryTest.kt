package com.example.android.architecture.blueprints.todoapp.data.source

import com.example.android.architecture.blueprints.todoapp.MainDispatcherRule
import com.example.android.architecture.blueprints.todoapp.data.Result
import com.example.android.architecture.blueprints.todoapp.data.Task
import com.example.android.architecture.blueprints.todoapp.data.succeeded
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class DefaultTasksRepositoryTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private val task1 = Task("Title1", "Description1")
    private val task2 = Task("Title2", "Description2")
    private val task3 = Task("Title3", "Description3")

    private val remoteTasks = listOf(task1, task2).sortedBy(Task::id)
    private val localTasks = listOf(task3).sortedBy(Task::id)

    private val tasksRemoteDataSource: TasksDataSource by lazy {
        FakeTasksDataSource(remoteTasks)
    }

    private val tasksLocalDataSource: TasksDataSource by lazy {
        FakeTasksDataSource(localTasks)
    }

    // Class under test
    private val tasksRepository: DefaultTasksRepository by lazy {
        DefaultTasksRepository(
            tasksRemoteDataSource,
            tasksLocalDataSource,
            Dispatchers.Main,
        )
    }

    @Test
    fun getTasks_requestsAllTasksFromRemoteDataSource() = runTest {
        // When tasks are requested from the tasks repository
        val result = tasksRepository.getTasks(true)

        // Then tasks are loaded from the remote data source
        assertThat(result.succeeded, `is`(true))
        result as Result.Success
        assertThat(result.data, `is`(remoteTasks))
    }
}
